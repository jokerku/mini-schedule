# mini-schedule

#### 介绍
基于  springBoot +ZooKeeper 的分布式任务调度框架，非常小巧，使用简单，只需要引入 maven 依赖，不需要单独部署服务端，做到了去中心化。确保所有任务在集群中不重复执行。支持动态添加和删除任务，支持任务分片功能。



#### 功能概述
1. 基于 SpringBoot + SpringTask + ZooKeeper 的分布式任务调度
2. 采用去中心化的设计，无需单独部署任务调度服务，降低使用成本
3. 确保同一任务在集群中不会重复执行
4. 当任务节点出现故障时，会将分配到该节点上的任务转移至其他节点执行
5. 支持动态添加、修改、启动和暂停任务
6. 任务失败告警、执行结果统计等（规划中）


#### 模块架构

![img.png](doc/img/miniSchedule架构.png)

#### 使用说明

1.  clone 项目到本地 并 `mvn clean install -pl com.jokerku:mini-schedule-springboot-starter`
2.  在你的项目 pom.xml 中 引入依赖

```
 <dependency>
            <groupId>com.jokerku</groupId>
            <artifactId>mini-schedule-springboot-starter</artifactId>
            <version>1.0.0-SNAPSHOT</version>
 </dependency>
```

3. 在 SpringBoot 启动类上添加`@EnableMiniScheduling`注解

```
@SpringBootApplication
@EnableMiniScheduling
public class ApiTestApplication {

    public static void main(String[] args) {
        SpringApplication.run(ApiTestApplication.class, args);
    }

}
```
4. 编写定时任务  
   1.在类上添加`@Component`注解，将该类交给 Spring 管理  
   2.在定时任务方法上添加 `@MiniScheduled` 注解

```
@Component("demoTaskOne")
public class DemoTaskOne {

    private static final Logger logger = LoggerFactory.getLogger(DemoTaskOne.class);

    @MiniScheduled(cron = "0/1 * * * * *", desc = "01定时任务执行测试：taskMethod01", autoStartUp = true, shardingTotalCount = 4)
    public void taskMethod01() {
        ShardingContext shardingContext = ShardingContext.getShardingContext();
        logger.info("taskMethod01, shardingItem{}", shardingContext.getShardingItem());
    }

    @MiniScheduled(cron = "0/10 * * * * *", desc = "01定时任务执行测试：taskMethod02", autoStartUp = true, shardingTotalCount = 1)
    public void taskMethod02() {
        ShardingContext shardingContext = ShardingContext.getShardingContext();
        logger.info("taskMethod02, shardingItem{}", shardingContext.getShardingItem());
    }
}
```
4.3 `@MiniScheduled` 注解参数如下

```
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface MiniScheduled {
    /**
     * 描述
     */
    String desc() default "";

    /**
     * cron 表达式
     */
    String cron() default "";

    /**
     * 分片总数
     */
    int shardingTotalCount() default 1;

    /**
     * 任务参数
     */
    String jobParameter() default "";

    /**
     * 是否自启动
     */
    boolean autoStartUp() default true;
}
```
4.4 在项目 application.yml 中 新增配置

```
mini-schedule:
  schedule-server-id: testJob
  schedule-server-name:  分布式任务测试
  zk-address: 127.0.0.1:2181
```
4.5 启动项目  
    1.如果部署了多个服务且任务的 `shardingTotalCount > 1`,则会自动进行分片  
    2.分片目前采用基于 IP 平均分配的方式   
    3.在任务方法中通过`ShardingContext shardingContext = ShardingContext.getShardingContext();`拿到当前的分片项  


#### 关于作者

1.  Joker(邮箱：jokerKu@foxmail.com wx:JokersKu)
2.  开源不易，如果该项目对您有帮助，在情况允许的情况下，可以对我一点点支持，万分感谢~!

weChat:
![img.png](doc/img/wechat.png)
aliPay:
![输入图片说明](doc/img/alipay.png)