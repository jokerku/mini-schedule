package com.jokerku.mini.schedule.service;

import javax.servlet.http.HttpServletRequest;

/**
 * @Author: guzq
 * @CreateTime: 2023/07/03 15:46
 * @Description: 首页
 * @Version: 1.0
 */
public interface IndexService {

    boolean login(HttpServletRequest request, String userName, String password);
}
