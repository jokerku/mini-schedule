package com.jokerku.mini.schedule.service.impl;

import com.jokerku.mini.schedule.domain.DataCollect;
import com.jokerku.mini.schedule.domain.DcsScheduleInfo;
import com.jokerku.mini.schedule.domain.DcsServerNode;
import com.jokerku.mini.schedule.domain.Instruct;
import com.jokerku.mini.schedule.export.DcsScheduleResource;
import com.jokerku.mini.schedule.export.IDcsScheduleResource;
import com.jokerku.mini.schedule.service.AdminService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Author: guzq
 * @CreateTime: 2023/05/31 10:50
 * @Description: 后台操作类
 * @Version: 1.0
 */
@Service
public class AdminServiceImpl implements AdminService {

    private volatile IDcsScheduleResource dcsScheduleResource;

    @Value("${mini-schedule.zk-address}")
    private String zkAddress;

    @Override
    public List<String> queryPathRootServerList() throws Exception {
        return getDcsScheduleResource().queryPathRootServerList();
    }

    @Override
    public List<DcsScheduleInfo> queryDcsScheduleInfoList(String schedulerServerId) throws Exception {
        return getDcsScheduleResource().queryDcsScheduleInfoList(schedulerServerId);
    }

    @Override
    public void pushInstruct(Instruct instruct) throws Exception {
        getDcsScheduleResource().pushInstruct(instruct);
    }

    @Override
    public DataCollect queryDataCollect() throws Exception {
        return getDcsScheduleResource().queryDataCollect();
    }

    @Override
    public List<DcsServerNode> queryDcsServerNodeList() throws Exception {
        return getDcsScheduleResource().queryDcsServerNodeList();
    }

    public IDcsScheduleResource getDcsScheduleResource() {
        if (null == dcsScheduleResource) {
            synchronized (this) {
                if (null == dcsScheduleResource) {
                    dcsScheduleResource = new DcsScheduleResource(zkAddress);
                }
            }
        }
        return dcsScheduleResource;
    }
}
