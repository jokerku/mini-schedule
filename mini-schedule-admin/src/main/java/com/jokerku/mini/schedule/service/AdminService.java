package com.jokerku.mini.schedule.service;

import com.jokerku.mini.schedule.domain.DataCollect;
import com.jokerku.mini.schedule.domain.DcsScheduleInfo;
import com.jokerku.mini.schedule.domain.DcsServerNode;
import com.jokerku.mini.schedule.domain.Instruct;

import java.util.List;

/**
 * @Author: guzq
 * @CreateTime: 2023/05/31 10:50
 * @Description: 后台操作服务
 * @Version: 1.0
 */
public interface AdminService {

    List<String> queryPathRootServerList() throws Exception;

    List<DcsScheduleInfo> queryDcsScheduleInfoList(String schedulerServerId) throws Exception;

    void pushInstruct(Instruct instruct) throws Exception;

    DataCollect queryDataCollect() throws Exception;

    List<DcsServerNode> queryDcsServerNodeList() throws Exception;

}
