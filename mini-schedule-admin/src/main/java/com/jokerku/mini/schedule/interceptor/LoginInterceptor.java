package com.jokerku.mini.schedule.interceptor;

import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * @Author: guzq
 * @CreateTime: 2023/07/03 16:14
 * @Description: 登录拦截器
 * @Version: 1.0
 */
@Component
public class LoginInterceptor implements HandlerInterceptor {

    /**
     * 目标方法执行前
     *
     * @param request
     * @param response
     * @param handler
     * @return
     * @throws Exception
     */
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        /*设置请求和响应的乱码 */
        request.setCharacterEncoding("UTF-8");
        response.setCharacterEncoding("UTF-8");

        // 获取请求的URL
        /*
         * indexOf() 方法可返回某个指定的字符串值在字符串中首次出现的位置。
         * 如果没有找到匹配的字符串则返回 -1。*/
        String url = request.getRequestURI();
        if (url.indexOf("/login") >= 0) {
            return true;
        }
        // 获取 session
        HttpSession session = request.getSession();
        Object obj = session.getAttribute("loginUser");

        //不等于null表示是已经登录了
        if (obj != null)
            return true;

        // 没有登录且不是登录页面，转发到登录页面，并给出提示错误信息
        request.setAttribute("msg", "还没登录，请先登录！");

        /*request.getRequestDispatcher("/toLogin").forward(request, response);
         * 1、属于转发，也是服务器跳转，相当于方法调用，在执行当前文件的过程中转向执行目标文件，两个文件(当前文件和目标文件)属于同一次请求，前后页共用一个request，可以通过此来传递一些数据或者session信息，request.setAttribute()和request.getAttribute()。
         * 2、在前后两次执行后，地址栏不变，仍是当前文件的地址。
         * 3、不能转向到本web应用之外的页面和网站，所以转向的速度要快。
         * 4、URL中所包含的“/”表示应用程序(项目)的路径。
         * */
        request.getRequestDispatcher("/login").forward(request, response);
        //response.sendRedirect("login");
        return false;
    }
}
