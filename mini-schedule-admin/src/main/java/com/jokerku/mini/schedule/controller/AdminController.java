package com.jokerku.mini.schedule.controller;

import com.alibaba.fastjson.JSON;
import com.jokerku.mini.schedule.domain.DcsScheduleInfo;
import com.jokerku.mini.schedule.domain.DcsServerNode;
import com.jokerku.mini.schedule.domain.EasyResult;
import com.jokerku.mini.schedule.domain.Instruct;
import com.jokerku.mini.schedule.service.AdminService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @Author: guzq
 * @CreateTime: 2023/05/31 14:31
 * @Description: 后台操作
 * @Version: 1.0
 */
@RestController
public class AdminController {

    private static final Logger logger = LoggerFactory.getLogger(AdminController.class);

    @Autowired
    private AdminService adminService;
    
    @RequestMapping("api/queryDcsServerNodeList")
    @ResponseBody
    public EasyResult queryDcsServerNodeList() {
        try {
            List<DcsServerNode> dcsServerNodeList = adminService.queryDcsServerNodeList();
            return EasyResult.buildEasyResultSuccess(dcsServerNodeList.size(), dcsServerNodeList);
        } catch (Exception e) {
            return EasyResult.buildEasyResultError(e);
        }
    }

    @RequestMapping("api/queryDcsScheduleInfoList")
    @ResponseBody
    public EasyResult queryDcsScheduleInfoList(@RequestParam String schedulerServerId) {
        try {
            logger.info("查询任务列表信息{}开始 ", schedulerServerId);
            List<DcsScheduleInfo> dcsScheduleInfoList = adminService.queryDcsScheduleInfoList(schedulerServerId);
            logger.info("查询任务列表信息{}完成 count：{}", schedulerServerId, dcsScheduleInfoList.size());
            return EasyResult.buildEasyResultSuccess(dcsScheduleInfoList.size(), dcsScheduleInfoList);
        } catch (Exception e) {
            logger.error("查询任务列表信息{}失败 ", schedulerServerId, e);
            return EasyResult.buildEasyResultError(e);
        }
    }

    @RequestMapping("api/queryPathRootServerList")
    @ResponseBody
    public EasyResult queryPathRootServerList() {
        try {
            List<String> serverList = adminService.queryPathRootServerList();
            return EasyResult.buildEasyResultSuccess(serverList.size(), serverList);
        } catch (Exception e) {
            return EasyResult.buildEasyResultError(e);
        }
    }

    @RequestMapping("api/queryDataCollect")
    @ResponseBody
    public EasyResult queryDataCollect() {
        try {
            return EasyResult.buildEasyResultSuccess(adminService.queryDataCollect());
        } catch (Exception e) {
            return EasyResult.buildEasyResultError(e);
        }
    }

    @RequestMapping("api/pushInstruct")
    @ResponseBody
    public EasyResult pushInstruct(@RequestParam String json) {
        try {
            Instruct instruct = JSON.parseObject(json, Instruct.class);
            if (null == instruct) return EasyResult.buildEasyResultError("json is null");
            adminService.pushInstruct(instruct);
            return EasyResult.buildEasyResultSuccess();
        } catch (Exception e) {
            return EasyResult.buildEasyResultError(e);
        }
    }
    
}
