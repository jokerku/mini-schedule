package com.jokerku.mini.schedule.service.impl;

import com.jokerku.mini.schedule.service.IndexService;
import com.jokerku.mini.schedule.util.IPUtil;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.HashSet;
import java.util.Set;

/**
 * @Author: guzq
 * @CreateTime: 2023/07/03 15:47
 * @Description: 首页
 * @Version: 1.0
 */
@Service
public class IndexServiceImpl implements IndexService {

    @Value("${mini-schedule.userName}")
    private String userName;
    @Value("${mini-schedule.password}")
    private String password;

    private static final Set<String> LOGIN_IP_CACHE = new HashSet<>();

    @Override
    public boolean login(HttpServletRequest request, String userName, String password) {
        if (userName.equals(this.userName) && password.equals(this.password)) {
            LOGIN_IP_CACHE.add(IPUtil.getIpAddr(request));
            return true;
        }
        return false;
    }
}
