import com.alibaba.fastjson.JSON;
import com.jokerku.mini.schedule.common.Constants;
import com.jokerku.mini.schedule.event.InstructEventListener;
import com.jokerku.mini.schedule.service.ZkCuratorServer;
import com.jokerku.mini.schedule.sharding.ShardingStrategy;
import com.jokerku.mini.schedule.sharding.ShardingStrategyFactory;
import com.jokerku.mini.schedule.util.ReflectionUtil;
import org.apache.curator.framework.CuratorFramework;

import java.lang.management.ManagementFactory;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @Author: guzq
 * @CreateTime: 2023/06/14 17:01
 * @Description: TODO
 * @Version: 1.0
 */
public class Test {

    @org.junit.Test
    public void testRandom() {
        for (int i = 0; i < 100; i++) {
            int index = new Random().nextInt(5);
            System.out.println(index);
        }
    }

    @org.junit.Test
    public void testPoll() {
        AtomicInteger atomicInteger = new AtomicInteger(Integer.MAX_VALUE - 1);

        for (int i = 0; i < 100; i++) {
            int andIncrement = atomicInteger.getAndIncrement();
            System.out.println(Math.abs(andIncrement % 5));
        }
    }

    @org.junit.Test
    public void testGetParentClassGenerics() throws InstantiationException, IllegalAccessException {
        Type[] parentClassGenerics = ReflectionUtil.getInterfaceGenerics(InstructEventListener.class);
        for (Type parentClassGeneric : parentClassGenerics) {
            System.out.println(parentClassGeneric.getTypeName());
        }
//        System.out.println(Arrays.toString(parentClassGenerics));
    }

    @org.junit.Test
    public void testManagementFactory() {
        String name = ManagementFactory.getRuntimeMXBean().getName();
        String s = name.split("@")[0];
        System.out.println(name);
        System.out.println(s);
    }


    @org.junit.Test
    public void testShardStrategy() {
        ShardingStrategy shardingStrategy = ShardingStrategyFactory.getShardingStrategy("AVERAGE_ALLOCATION");
        List<String> ipList = new ArrayList<>();
        ipList.add("100.111.222.333");
        ipList.add("200.111.222.333");
        ipList.add("300.111.222.333");
        Map<String, List<Integer>> sharding = shardingStrategy.sharding(ipList, 1);
        System.out.println(sharding);
    }

    @org.junit.Test
    public void testDelZKNode() throws Exception {
//        String path = "/mini-schedule/server/testJob/runningTask/demoTaskOne-taskMethod01/61.237.8.88";
        String path = "/mini-schedule/server/testJob/runningTask/demoTaskOne-taskMethod01/61.237.8.11111";
        CuratorFramework client = ZkCuratorServer.getClient("127.0.0.1:2181");
        ZkCuratorServer.appendPersistentData(client, path, JSON.toJSONString("11231312"));

//        ZkCuratorServer.createNodeSimple(client, path);
//        ZkCuratorServer.setData(client, path, "123123");
//        ZkCuratorServer.deletingChildrenIfNeeded(client, path);
        TimeUnit.SECONDS.sleep(10);
        System.out.println("开始删除");
        ZkCuratorServer.deletingChildrenIfNeeded(client, path);

    }
}
