package com.jokerku.mini.schedule.sharding;

/**
 * @Author: guzq
 * @CreateTime: 2023/06/19 19:47
 * @Description: 分片上下文
 * @Version: 1.0
 */
public class ShardingContext {

    /**
     * 作业任务ID.
     */
    private String taskId;

    /**
     * 作业名称.
     */
    private String jobName;

    /**
     * 分片总数.
     */
    private int shardingTotalCount;

    /**
     * 分配于本作业实例的分片项.
     */
    private int shardingItem;

    /**
     * 作业自定义参数.
     * 可以配置多个相同的作业, 但是用不同的参数作为不同的调度实例.
     */
    private String jobParameter;

    public ShardingContext(String taskId, String jobName, int shardingTotalCount, int shardingItem, String jobParameter) {
        this.taskId = taskId;
        this.jobName = jobName;
        this.shardingTotalCount = shardingTotalCount;
        this.shardingItem = shardingItem;
        this.jobParameter = jobParameter;
    }

    public String getTaskId() {
        return taskId;
    }

    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }

    public String getJobName() {
        return jobName;
    }

    public void setJobName(String jobName) {
        this.jobName = jobName;
    }

    public int getShardingTotalCount() {
        return shardingTotalCount;
    }

    public void setShardingTotalCount(int shardingTotalCount) {
        this.shardingTotalCount = shardingTotalCount;
    }

    public String getJobParameter() {
        return jobParameter;
    }

    public void setJobParameter(String jobParameter) {
        this.jobParameter = jobParameter;
    }

    public int getShardingItem() {
        return shardingItem;
    }

    public void setShardingItem(int shardingItem) {
        this.shardingItem = shardingItem;
    }

    private static final InheritableThreadLocal<ShardingContext> contextHolder = new InheritableThreadLocal<>();

    public static ShardingContext getShardingContext() {
        return contextHolder.get();
    }

    public static void setShardingContext(ShardingContext shardingContext) {
        contextHolder.set(shardingContext);
    }
}
