package com.jokerku.mini.schedule.util;

import java.util.concurrent.TimeUnit;

/**
 * @Author: guzq
 * @CreateTime: 2023/06/14 15:29
 * @Description: 线程睡眠工具类
 * @Version: 1.0
 */
public class SleepUtil {

    public static void sleep() {
        try {
            TimeUnit.MILLISECONDS.sleep(100);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }
}
