package com.jokerku.mini.schedule.domain;

/**
 * @Author: guzq
 * @CreateTime: 2023/05/31 14:49
 * @Description: TODO
 * @Version: 1.0
 */
public class DcsServerNode {

    private String schedulerServerId;       //任务服务ID
    private String schedulerServerName;     //任务服务名称

    public DcsServerNode(String schedulerServerId, String schedulerServerName) {
        this.schedulerServerId = schedulerServerId;
        this.schedulerServerName = schedulerServerName;
    }

    public String getSchedulerServerId() {
        return schedulerServerId;
    }

    public void setSchedulerServerId(String schedulerServerId) {
        this.schedulerServerId = schedulerServerId;
    }

    public String getSchedulerServerName() {
        return schedulerServerName;
    }

    public void setSchedulerServerName(String schedulerServerName) {
        this.schedulerServerName = schedulerServerName;
    }
}
