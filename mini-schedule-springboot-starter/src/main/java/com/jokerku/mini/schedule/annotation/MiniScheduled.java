package com.jokerku.mini.schedule.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @Author: guzq
 * @CreateTime: 2023/05/24 15:34
 * @Description: 定时任务配置类
 * @Version: 1.0
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface MiniScheduled {

    /**
     * 描述
     */
    String desc() default "";

    /**
     * cron 表达式
     */
    String cron() default "";

    /**
     * 分片总数
     */
    int shardingTotalCount() default 1;

    /**
     * 任务参数
     */
    String jobParameter() default "";

    /**
     * 是否自启动
     */
    boolean autoStartUp() default true;
}
