package com.jokerku.mini.schedule.loadbalance;

/**
 * @Author: guzq
 * @CreateTime: 2023/06/14 17:18
 * @Description: 负载均衡策略
 * @Version: 1.0
 */
public enum LoadBalanceStrategy {

    Random,
    Poll,
    ;
}
