package com.jokerku.mini.schedule.util;

/**
 * @Author: guzq
 * @CreateTime: 2023/05/25 10:18
 * @Description: 字符串工具类
 * @Version: 1.0
 */
public class StrUtil {

    public static String join(String... str) {
        StringBuilder sb = new StringBuilder();
        for (String s : str) {
            if (s != null) {
                sb.append(s);
            }
        }
        return sb.toString();
    }
}
