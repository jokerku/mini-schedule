package com.jokerku.mini.schedule.loadbalance;

import java.util.List;

/**
 * @Author: guzq
 * @CreateTime: 2023/06/14 16:03
 * @Description: 负责均衡策略
 * @Version: 1.0
 */
public interface LoadBalance {

    String getDispatchIp(List<String> ipList);

    LoadBalanceStrategy getStrategy();
}
