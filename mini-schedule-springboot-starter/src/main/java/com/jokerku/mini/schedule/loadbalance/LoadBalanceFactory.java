package com.jokerku.mini.schedule.loadbalance;

import com.jokerku.mini.schedule.loadbalance.impl.PollLoadBalance;
import com.jokerku.mini.schedule.loadbalance.impl.RandomLoadBalance;

import java.util.HashMap;
import java.util.Map;

/**
 * @Author: guzq
 * @CreateTime: 2023/06/14 17:19
 * @Description: 负载均衡工厂
 * @Version: 1.0
 */

public class LoadBalanceFactory {

    private static final Map<LoadBalanceStrategy, LoadBalance> LOAD_BALANCE_MAP = new HashMap<>();

    static {
        LOAD_BALANCE_MAP.put(LoadBalanceStrategy.Poll, new PollLoadBalance());
        LOAD_BALANCE_MAP.put(LoadBalanceStrategy.Random, new RandomLoadBalance());
    }

    public static LoadBalance getLoadBalanceService(LoadBalanceStrategy strategy) {
        if (null == strategy) {
            throw new RuntimeException("strategy cannot be null");
        }
        return LOAD_BALANCE_MAP.get(strategy);
    }
}
