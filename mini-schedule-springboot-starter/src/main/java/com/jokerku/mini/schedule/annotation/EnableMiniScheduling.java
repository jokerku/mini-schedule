package com.jokerku.mini.schedule.annotation;

import com.jokerku.mini.schedule.config.MiniScheduleConfiguration;
import com.jokerku.mini.schedule.task.CronTaskRegister;
import com.jokerku.mini.schedule.task.ScheduleConfig;
import org.springframework.boot.autoconfigure.ImportAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @Author: guzq
 * @CreateTime: 2023/05/24 15:36
 * @Description: 启用 Mini-Schedule 定时任务注解,在调用方启动类上使用
 * @Version: 1.0
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@Import(MiniScheduleConfiguration.class)
@ImportAutoConfiguration({ScheduleConfig.class, CronTaskRegister.class})
@ComponentScan(basePackages = "com.jokerku.mini.schedule.*")
public @interface EnableMiniScheduling {
}
