package com.jokerku.mini.schedule.util;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

/**
 * @Author: guzq
 * @CreateTime: 2023/06/19 15:22
 * @Description: 反射工具类
 * @Version: 1.0
 */
public class ReflectionUtil {

    /**
     * 获取父类上的泛型
     *
     * @param targetClass 目标类
     * @return 父类上的泛型
     */
    public static Type getParentClassGenerics(Class<?> targetClass) {
        Type superClass = targetClass.getGenericSuperclass();
        if (superClass instanceof ParameterizedType) {
            ParameterizedType parameterizedType = (ParameterizedType) superClass;
            Type[] typeArgs = parameterizedType.getActualTypeArguments();
            Type typeArg = typeArgs[0];
            return typeArg;
        }
        return null;
    }

    public static Type[] getInterfaceGenerics(Class<?> targetClass) {
        Type[] genericInterfaces = targetClass.getGenericInterfaces();
        for (Type genericInterface : genericInterfaces) {
            if (genericInterface instanceof ParameterizedType) {
                ParameterizedType parameterizedType = (ParameterizedType) genericInterface;
                return parameterizedType.getActualTypeArguments();
            }
        }
        return new Type[]{};
    }
}
