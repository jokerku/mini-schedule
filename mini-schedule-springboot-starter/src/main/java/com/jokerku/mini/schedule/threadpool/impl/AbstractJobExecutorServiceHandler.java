package com.jokerku.mini.schedule.threadpool.impl;

import com.jokerku.mini.schedule.threadpool.ExecutorServiceObj;
import com.jokerku.mini.schedule.threadpool.JobExecutorServiceHandler;

import java.util.concurrent.ExecutorService;

/**
 * @Description AbstractJobExecutorServiceHandler
 * @Author Joker Ku
 * @Date 2023/7/2 12:44
 * @Version 1.0
 */
public abstract class AbstractJobExecutorServiceHandler implements JobExecutorServiceHandler {

    @Override
    public ExecutorService createExecutorService(String jobName) {
        return new ExecutorServiceObj(jobName, getCorePoolSize()).getExecutorService();
    }

    protected abstract int getCorePoolSize();
}
