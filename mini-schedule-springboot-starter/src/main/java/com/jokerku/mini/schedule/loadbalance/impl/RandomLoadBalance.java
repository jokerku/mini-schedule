package com.jokerku.mini.schedule.loadbalance.impl;

import com.jokerku.mini.schedule.loadbalance.LoadBalance;
import com.jokerku.mini.schedule.loadbalance.LoadBalanceStrategy;
import org.apache.commons.collections.CollectionUtils;

import java.util.List;
import java.util.Random;

/**
 * @Author: guzq
 * @CreateTime: 2023/06/14 16:39
 * @Description: 随机负载
 * @Version: 1.0
 */
public class RandomLoadBalance implements LoadBalance {

    @Override
    public String getDispatchIp(List<String> ipList) {
        if (CollectionUtils.isEmpty(ipList)) {
            throw new RuntimeException("Ip list cannot be empty");
        }
        int index = new Random().nextInt(ipList.size());
        return ipList.get(index);
    }

    @Override
    public LoadBalanceStrategy getStrategy() {
        return LoadBalanceStrategy.Random;
    }
}
