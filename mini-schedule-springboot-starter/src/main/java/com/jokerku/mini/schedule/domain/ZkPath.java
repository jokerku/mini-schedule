package com.jokerku.mini.schedule.domain;

import com.jokerku.mini.schedule.common.Constants;
import com.jokerku.mini.schedule.util.StrUtil;

/**
 * @Author: guzq
 * @CreateTime: 2023/05/25 18:55
 * @Description: ZK路径
 * @Version: 1.0
 */
public class ZkPath {

    /*
        /mini-schedule/server/mini-schedule-spring-boot-starter-test/exec
        /mini-schedule/server/mini-schedule-spring-boot-starter-test/master/instance
        /mini-schedule/server/mini-schedule-spring-boot-starter-test/runningTask
        /mini-schedule/server/mini-schedule-spring-boot-starter-test/runningIp
                                                                        /xxx.xxx.123.x
                                                                        /xxx.xxx.124.x
        /mini-schedule/server/mini-schedule-spring-boot-starter-test/tasks
                                                                        /xxx.xxx.123.x
                                                                                        /class
                                                                                                /xxxClass
                                                                                                            /method
                                                                                                                    /xxxMethod1
                                                                                                                                /status
                                                                                                                                /value
                                                                                                                    /xxxMethod2
                                                                                                                                /status
                                                                                                                                /value
     */

    private ZkPath() {

    }

    public static String getBasePath() {
        // /mini-schedule/server/mini-schedule-spring-boot-starter-test
        return StrUtil.join(Constants.Global.rootPath, Constants.Global.LINE, "server", Constants.Global.LINE, Constants.Global.scheduleServerId);
    }

    public static String geExecPath() {
        // 后台操作目录
        // /mini-schedule/server/mini-schedule-spring-boot-starter-test/exec
        return StrUtil.join(getBasePath(), Constants.Global.LINE, "exec");
    }

    public static String getLockPath() {
        // /mini-schedule/server/mini-schedule-spring-boot-starter-test/lock
        return StrUtil.join(getBasePath(), Constants.Global.LINE, "lock");
    }

    public static String getMasterPath() {
        // /mini-schedule/server/mini-schedule-spring-boot-starter-test/master
        return StrUtil.join(getBasePath(), Constants.Global.LINE, "master");
    }

    public static String getMasterInstancePath() {
        // /mini-schedule/server/mini-schedule-spring-boot-starter-test/master/instance
        return StrUtil.join(getMasterPath(), Constants.Global.LINE, "instance");
    }

    public static String getServerRunningIpPath() {
        // /mini-schedule/server/mini-schedule-spring-boot-starter-test/runningIp/xxx.xxx.xx.xxx
        return StrUtil.join(getBasePath(), Constants.Global.LINE, "runningIp", Constants.Global.LINE, Constants.Global.ip);
    }

    public static String getServerTaskPath() {
        // /mini-schedule/server/mini-schedule-spring-boot-starter-test/tasks/xxx.xxx.xx.xxx
        return StrUtil.join(getBasePath(), Constants.Global.LINE, "tasks", Constants.Global.LINE, Constants.Global.ip);
    }

    public static String getRunningTaskPath(String taskId) {
        // /mini-schedule/server/mini-schedule-spring-boot-starter-test/runningTask/taskId
        return StrUtil.join(getBasePath(), Constants.Global.LINE, "runningTask", Constants.Global.LINE, taskId);
    }

    public static String getRunningTaskIpPath(String taskId, String ip) {
        // /mini-schedule/server/mini-schedule-spring-boot-starter-test/runningTask/taskId/xxx.xxx.xx
        return StrUtil.join(getBasePath(), Constants.Global.LINE, "runningTask", Constants.Global.LINE, taskId, Constants.Global.LINE, ip);
    }

    public static String getServerTaskClassPath(ExecOrder execOrder) {
        // /mini-schedule/server/mini-schedule-spring-boot-starter-test/tasks/xxx.xxx.xx.xxx/class/className
        return StrUtil.join(getServerTaskPath(), Constants.Global.LINE, "class", Constants.Global.LINE, execOrder.getBeanName());
    }

    public static String getServerTaskClassMethodPath(ExecOrder execOrder) {
        // /mini-schedule/server/mini-schedule-spring-boot-starter-test/tasks/xxx.xxx.xx.xxx/class/className/method/methodName
        return StrUtil.join(getServerTaskClassPath(execOrder), Constants.Global.LINE, "method", Constants.Global.LINE, execOrder.getMethodName());
    }

    public static String getServerTaskClassMethodValuePath(ExecOrder execOrder) {
        // /mini-schedule/server/mini-schedule-spring-boot-starter-test/tasks/xxx.xxx.xx.xxx/class/className/method/methodName/value
        return StrUtil.join(getServerTaskClassMethodPath(execOrder), Constants.Global.LINE, "value");
    }

    public static String getServerTaskClassMethodStatusPath(ExecOrder execOrder) {
        // /mini-schedule/server/mini-schedule-spring-boot-starter-test/tasks/xxx.xxx.xx.xxx/class/className/method/methodName/status
        return StrUtil.join(getServerTaskClassMethodPath(execOrder), Constants.Global.LINE, "status");
    }

    public static String getShardingPath() {
        // /mini-schedule/server/mini-schedule-spring-boot-starter-test/master/sharding
        return StrUtil.join(getMasterPath(), Constants.Global.LINE, "sharding");
    }

    public static String getShardingNecessaryPath() {
        // /mini-schedule/server/mini-schedule-spring-boot-starter-test/master/sharding/necessary
        return StrUtil.join(getShardingPath(), Constants.Global.LINE, "necessary");
    }

    public static String getShardingProcessingPath() {
        // /mini-schedule/server/mini-schedule-spring-boot-starter-test/master/sharding/processing
        return StrUtil.join(getShardingPath(), Constants.Global.LINE, "processing");
    }

    public static String getShardingLockPath() {
        // /mini-schedule/server/mini-schedule-spring-boot-starter-test/master/sharding/lock
        return StrUtil.join(getShardingPath(), Constants.Global.LINE, "lock");
    }

    // --------------- build ---------------
    public static String buildRootServerPath(String scheduleServerId) {
        // /mini/schedule/server/mini-schedule-spring-boot-starter-test
        return StrUtil.join(Constants.Global.rootPath, Constants.Global.LINE, "server", Constants.Global.LINE, scheduleServerId);
    }

    public static String buildServerRunningIpPath(String scheduleServerId) {
        // /mini/schedule/server/mini-schedule-spring-boot-starter-test/runningIp
        return StrUtil.join(buildRootServerPath(scheduleServerId), Constants.Global.LINE, "runningIp");
    }

    public static String buildServerRunningIpPathValue(String scheduleServerId, String ip) {
        // /mini/schedule/server/mini-schedule-spring-boot-starter-test/runningIp/xxx.xxx.xx
        return StrUtil.join(buildRootServerPath(scheduleServerId), Constants.Global.LINE, "runningIp", Constants.Global.LINE, ip);
    }

    public static String buildServerTaskPath(String scheduleServerId) {
        // /mini/schedule/server/mini-schedule-spring-boot-starter-test/tasks
        return StrUtil.join(buildRootServerPath(scheduleServerId), Constants.Global.LINE, "tasks");
    }

    public static String buildServerTaskValuePath(String scheduleServerId, String ip) {
        // /mini/schedule/server/mini-schedule-spring-boot-starter-test/tasks/100.xx.129.xx
        return StrUtil.join(buildServerTaskPath(scheduleServerId), Constants.Global.LINE, ip);
    }

    public static String buildServerTaskClassPath(String scheduleServerId, String ip) {
        // /mini/schedule/server/mini-schedule-spring-boot-starter-test/tasks/100.xx.129.xx/class
        return StrUtil.join(buildServerTaskPath(scheduleServerId), Constants.Global.LINE, ip, Constants.Global.LINE, "class");
    }

    public static String buildServerTaskClassMethodPath(String schedulerServerId, String ip, String className) {
        // /mini/schedule/server/mini-schedule-spring-boot-starter-test/tasks/100.xx.129.xx/class/xxxClass/method
        return StrUtil.join(buildServerTaskClassPath(schedulerServerId, ip), Constants.Global.LINE, className, Constants.Global.LINE, "method");

    }

    public static String buildServerTaskClassMethodValuePath(String schedulerServerId, String ip, String className, String method) {
        // /mini/schedule/server/mini-schedule-spring-boot-starter-test/tasks/100.xx.129.xx/class/xxxClass/method/taskMethod02/value
        return StrUtil.join(buildServerTaskClassMethodPath(schedulerServerId, ip, className), Constants.Global.LINE, method, Constants.Global.LINE, "value");
    }

    public static String buildServerTaskClassMethodStatusPath(String schedulerServerId, String ip, String className, String method) {
        // /mini/schedule/server/mini-schedule-spring-boot-starter-test/tasks/100.xx.129.xx/class/xxxClass/method/taskMethod02/status
        return StrUtil.join(buildServerTaskClassMethodPath(schedulerServerId, ip, className), Constants.Global.LINE, method, Constants.Global.LINE,  "status");
    }
}
