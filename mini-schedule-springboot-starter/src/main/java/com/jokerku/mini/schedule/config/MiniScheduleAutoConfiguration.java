package com.jokerku.mini.schedule.config;

import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * @Author: guzq
 * @CreateTime: 2023/05/24 16:33
 * @Description: 自动配置类
 * @Version: 1.0
 */
@Configuration("mini-schedule-auto-configuration")
@EnableConfigurationProperties(value = MiniScheduleConfigProperties.class)
public class MiniScheduleAutoConfiguration {

}
