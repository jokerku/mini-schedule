package com.jokerku.mini.schedule.common;

import com.jokerku.mini.schedule.domain.ExecOrder;
import com.jokerku.mini.schedule.leader.LeaderService;
import com.jokerku.mini.schedule.task.ScheduledTask;
import org.apache.curator.framework.CuratorFramework;
import org.springframework.context.ApplicationContext;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @Author: guzq
 * @CreateTime: 2023/05/24 15:46
 * @Description: 常量类
 * @Version: 1.0
 */
public class Constants {

    /**
     * 任务列表 beanName -> List<ExecOrder>
     */
    public static final Map<String, List<ExecOrder>> EXEC_ORDER_MAP = new ConcurrentHashMap<>(64);
    /**
     * 执行中的定时任务 taskId -> ScheduledTask
     */
    public static final Map<String, ScheduledTask> SCHEDULED_TASK_MAP = new ConcurrentHashMap<>(64);

    public static class Global {
        public static ApplicationContext applicationContext;
        /**
         * 本机 ip
         */
        public static String ip;
        /**
         * zk 地址
         */
        public static String zkAddress;
        /**
         * 任务服务Id
         */
        public static String scheduleServerId;
        /**
         * 任务服务名
         */
        public static String scheduleServerName;
        /**
         * 分割线
         */
        public final static String LINE = "/";
        /**
         * 横线
         */
        public final static String HORIZONTAL_LINE = "-";
        /**
         * 字符集编码
         */
        public final static Charset CHARSET = StandardCharsets.UTF_8;

        /**
         * ZK客户端
         */
        public static CuratorFramework client;
        /**
         * ZK根目录
         */
        public static String rootPath = "/mini-schedule";
        /**
         * 定时任务线程池线程数大小
         */
        public static int schedulePoolSize = 8;

        public static LeaderService leaderService;
    }

    /**
     * 后台操作命令事件
     */
    public static class InstructStatus {

        /**
         * 停止
         */
        public static final int STOP = 0;
        /**
         * 启动
         */
        public static final int START = 1;
        /**
         * 刷新
         */
        public static final int REFRESH = 2;

    }

}
