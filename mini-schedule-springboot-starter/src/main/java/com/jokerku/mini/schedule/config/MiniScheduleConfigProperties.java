package com.jokerku.mini.schedule.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @Author: guzq
 * @CreateTime: 2023/05/24 16:31
 * @Description: 配置属性
 * @Version: 1.0
 */
@ConfigurationProperties(prefix = "mini-schedule")
@Component("mini-schedule-config-properties")
public class MiniScheduleConfigProperties {
    /**
     * zk 地址
     */
    private String zkAddress;
    /**
     * 任务服务Id
     */
    private String scheduleServerId;
    /**
     * 任务服务名
     */
    private String scheduleServerName;

    public String getZkAddress() {
        return zkAddress;
    }

    public void setZkAddress(String zkAddress) {
        this.zkAddress = zkAddress;
    }

    public String getScheduleServerId() {
        return scheduleServerId;
    }

    public void setScheduleServerId(String scheduleServerId) {
        this.scheduleServerId = scheduleServerId;
    }

    public String getScheduleServerName() {
        return scheduleServerName;
    }

    public void setScheduleServerName(String scheduleServerName) {
        this.scheduleServerName = scheduleServerName;
    }
}
