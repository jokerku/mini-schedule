package com.jokerku.mini.schedule.event;


import java.util.EventListener;

/**
 * @Author: guzq
 * @CreateTime: 2023/06/19 14:35
 * @Description: 抽象事件监听器
 * @Version: 1.0
 */
public interface ScheduleEventListener<T> extends EventListener {

    void consumeEvent(T event);
}
