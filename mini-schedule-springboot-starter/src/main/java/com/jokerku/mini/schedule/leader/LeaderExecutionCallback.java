package com.jokerku.mini.schedule.leader;

/**
 * @Author: guzq
 * @CreateTime: 2023/06/01 23:12
 * @Description: leader 选举结果回调
 * @Version: 1.0
 */
public interface LeaderExecutionCallback {

    void callback() throws Exception;
}
