package com.jokerku.mini.schedule.task;

import com.jokerku.mini.schedule.common.Constants;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;

/**
 * @Author: guzq
 * @CreateTime: 2023/05/25 11:16
 * @Description: 定时任务配置类
 * @Version: 1.0
 */
@Configuration("mini-schedule-task-config")
public class ScheduleConfig {

    @Bean("mini-schedule-task-scheduler")
    public TaskScheduler taskScheduler() {
        ThreadPoolTaskScheduler taskScheduler = new ThreadPoolTaskScheduler();
        taskScheduler.setPoolSize(Constants.Global.schedulePoolSize);
        taskScheduler.setRemoveOnCancelPolicy(true);
        taskScheduler.setThreadNamePrefix("mini-schedule-");

        return taskScheduler;
    }
}
