package com.jokerku.mini.schedule.event;

import com.jokerku.mini.schedule.common.Constants;
import com.jokerku.mini.schedule.sharding.ShardingService;
import com.jokerku.mini.schedule.task.ScheduleExec;
import org.apache.curator.framework.recipes.cache.TreeCacheEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @Author: guzq
 * @CreateTime: 2023/06/01 23:54
 * @Description: 主节点目录事件
 * @Version: 1.0
 */
public class LeaderEventListener implements ScheduleEventListener<TreeCacheEvent> {

    private static final Logger logger = LoggerFactory.getLogger(LeaderEventListener.class);

    @Override
    public void consumeEvent(TreeCacheEvent treeCacheEvent) {
        try {
            switch ((treeCacheEvent).getType()) {
                // 节点添加/修改事件
                case NODE_ADDED:
                case NODE_UPDATED: {
                    break;
                }
                case NODE_REMOVED: {
                    logger.info("Listening to leader node remove event");
                    Constants.Global.leaderService.selectLeader();
//                    ShardingService.getInstance().setReShardingFlag();
//                    ShardingService.getInstance().shardingIfNecessary();
                    break;
                }
                default:
                    break;
            }
        } catch (Exception e) {
            logger.error("Consume leader event error", e);
        }
    }
}
