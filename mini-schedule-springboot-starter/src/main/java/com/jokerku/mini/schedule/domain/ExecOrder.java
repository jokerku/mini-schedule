package com.jokerku.mini.schedule.domain;

import com.jokerku.mini.schedule.common.Constants;

/**
 * @Author: guzq
 * @CreateTime: 2023/05/24 16:07
 * @Description: 定时任务属性包装类
 * @Version: 1.0
 */
public class ExecOrder {

    /**
     * 任务对象
     */
    private Object bean;
    /**
     * 对象名
     */
    private String beanName;
    /**
     * 方法名
     */
    private String methodName;
    /**
     * cron表达式
     */
    private String cron;
    /**
     * 任务状态
     */
    private Boolean autoStartup;
    /**
     * 任务描述
     */
    private String desc;
    /**
     * 分片总数
     */
    private int shardingTotalCount;
    /**
     * 任务自定义参数
     */
    private String jobParameter;

    public Object getBean() {
        return bean;
    }

    public void setBean(Object bean) {
        this.bean = bean;
    }

    public String getBeanName() {
        return beanName;
    }

    public void setBeanName(String beanName) {
        this.beanName = beanName;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public String getCron() {
        return cron;
    }

    public void setCron(String cron) {
        this.cron = cron;
    }

    public Boolean getAutoStartup() {
        return autoStartup;
    }

    public void setAutoStartup(Boolean autoStartup) {
        this.autoStartup = autoStartup;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public int getShardingTotalCount() {
        return shardingTotalCount;
    }

    public void setShardingTotalCount(int shardingTotalCount) {
        this.shardingTotalCount = shardingTotalCount;
    }

    public String getJobParameter() {
        return jobParameter;
    }

    public void setJobParameter(String jobParameter) {
        this.jobParameter = jobParameter;
    }

    public String getTaskId() {
        return getBeanName() + Constants.Global.HORIZONTAL_LINE + getMethodName();
    }
}
