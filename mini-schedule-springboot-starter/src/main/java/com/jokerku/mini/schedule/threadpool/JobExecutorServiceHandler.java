package com.jokerku.mini.schedule.threadpool;

import java.util.concurrent.ExecutorService;

/**
 *
 */
public interface JobExecutorServiceHandler {

    ExecutorService createExecutorService(final String jobName);

    String getType();

}
