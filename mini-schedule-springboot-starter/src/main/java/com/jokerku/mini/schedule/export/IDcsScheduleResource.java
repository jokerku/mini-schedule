package com.jokerku.mini.schedule.export;

import com.jokerku.mini.schedule.domain.DataCollect;
import com.jokerku.mini.schedule.domain.DcsScheduleInfo;
import com.jokerku.mini.schedule.domain.DcsServerNode;
import com.jokerku.mini.schedule.domain.Instruct;

import java.util.List;

/**
 * @Author: guzq
 * @CreateTime: 2023/05/31 17:44
 * @Description: 后台操作类
 * @Version: 1.0
 */
public interface IDcsScheduleResource {

    List<String> queryPathRootServerList() throws Exception;

    List<DcsScheduleInfo> queryDcsScheduleInfoList(String schedulerServerId) throws Exception;

    void pushInstruct(Instruct instruct) throws Exception;

    DataCollect queryDataCollect() throws Exception;

    List<DcsServerNode> queryDcsServerNodeList() throws Exception;
}
