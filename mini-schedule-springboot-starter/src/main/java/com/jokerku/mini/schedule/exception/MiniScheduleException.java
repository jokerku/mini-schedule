package com.jokerku.mini.schedule.exception;

/**
 * @Author: guzq
 * @CreateTime: 2023/06/21 15:08
 * @Description: 任务异常
 * @Version: 1.0
 */
public class MiniScheduleException extends RuntimeException {

    public MiniScheduleException() {
    }

    public MiniScheduleException(String message) {
        super(message);
    }

    public MiniScheduleException(String message, Throwable cause) {
        super(message, cause);
    }
}
