package com.jokerku.mini.schedule.sharding;

import java.util.List;
import java.util.Map;

/**
 * @Author: guzq
 * @CreateTime: 2023/06/20 11:28
 * @Description: 分片策略
 * @Version: 1.0
 */
public interface ShardingStrategy {

    Map<String, List<Integer>> sharding(final List<String> ipList, final int shardingTotalCount);

    String getType();
}
