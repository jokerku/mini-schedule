package com.jokerku.mini.schedule.spi;


import com.google.common.collect.Lists;
import com.jokerku.mini.schedule.exception.MiniScheduleException;

import java.util.List;
import java.util.ServiceLoader;

/**
 * @Author: guzq
 * @CreateTime: 2023/07/04 10:44
 * @Description: spi加载
 * @Version: 1.0
 */
public class JobServiceLoader {

    public static <T> List<T> load(Class<T> clazz) {
        return load(clazz, null);
    }

    public static <T> List<T> load(Class<T> clazz, ClassLoader classLoader) {
        if (null == clazz) {
            throw new MiniScheduleException("Load service error, class cannot be null");
        }
        if (null == classLoader) {
            classLoader = Thread.currentThread().getContextClassLoader();
        }
        ServiceLoader<T> serviceLoader = ServiceLoader.load(clazz, classLoader);
        return Lists.newArrayList(serviceLoader);
    }
}
