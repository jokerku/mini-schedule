package com.jokerku.mini.schedule.task;

import java.util.concurrent.ScheduledFuture;

/**
 * @Author: guzq
 * @CreateTime: 2023/05/25 11:41
 * @Description: 定时任务
 * @Version: 1.0
 */
public class ScheduledTask {

    volatile ScheduledFuture<?> future;

    /**
     * 取消定时任务
     */
    public void cancel() {
        ScheduledFuture<?> future = this.future;
        if (null == future) return;
        future.cancel(true);
    }

    /**
     * 定时任务是否已取消
     *
     * @return true:已取消
     */
    public boolean isCanceled() {
        ScheduledFuture<?> future = this.future;
        if (null == future) return true;
        return future.isCancelled();
    }

}
