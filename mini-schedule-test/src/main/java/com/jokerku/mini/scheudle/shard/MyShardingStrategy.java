package com.jokerku.mini.scheudle.shard;

import com.jokerku.mini.schedule.sharding.ShardingStrategy;

import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * @Author: guzq
 * @CreateTime: 2023/07/04 10:57
 * @Description: TODO
 * @Version: 1.0
 */
public class MyShardingStrategy implements ShardingStrategy {
    @Override
    public Map<String, List<Integer>> sharding(List<String> ipList, int shardingTotalCount) {
        return Collections.emptyMap();
    }

    @Override
    public String getType() {
        return "CUSTOM";
    }
}
