package com.jokerku.mini.scheudle.task;

import com.alibaba.fastjson.JSON;
import com.jokerku.mini.schedule.annotation.MiniScheduled;
import com.jokerku.mini.schedule.sharding.ShardingContext;
import com.jokerku.mini.schedule.task.ScheduleRunnable;
import org.aspectj.weaver.ast.Var;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;


@Component("demoTaskOne")
public class DemoTaskOne {

    private static final Logger logger = LoggerFactory.getLogger(DemoTaskOne.class);


    @MiniScheduled(cron = "0/1 * * * * *", desc = "01定时任务执行测试：taskMethod01", autoStartUp = true, shardingTotalCount = 4)
    public void taskMethod01() {
        ShardingContext shardingContext = ShardingContext.getShardingContext();
        logger.info("taskMethod01, shardingItem{}", shardingContext.getShardingItem());
//        throw new RuntimeException("Manual exception");
//        try {
//            Thread.sleep(3000);
//        } catch (InterruptedException ignored) {
//        }
    }

        @MiniScheduled(cron = "0/1 * * * * *", desc = "01定时任务执行测试：taskMethod02", autoStartUp = true, shardingTotalCount = 1)
    public void taskMethod02() {
            ShardingContext shardingContext = ShardingContext.getShardingContext();
            logger.info("taskMethod02, shardingItem{}", shardingContext.getShardingItem());
//        try {
//            Thread.sleep(5000);
//        } catch (InterruptedException ignored) {
//        }
    }


}
