package com.jokerku.mini.scheudle.task;

import com.jokerku.mini.schedule.annotation.MiniScheduled;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;


@Component("demoTaskTwo")
public class DemoTaskTwo {
    private static final Logger logger = LoggerFactory.getLogger(DemoTaskTwo.class);

    //    @MiniScheduled(cron = "0 * * * * *", desc = "02定时任务执行测试：taskMethod01")
//    @MiniScheduled(cron = "0/2 * * * * *", desc = "02定时任务执行测试：taskMethod01", autoStartUp = true)
    public void taskMethod01() {
        logger.info("{} 02定时任务执行测试", Thread.currentThread().getName());
    }

    //    @MiniScheduled(cron = "*/10 * * * * *", desc = "02定时任务执行测试：taskMethod02")
    public void taskMethod02() {
        System.out.println("02定时任务执行测试：taskMethod02");
    }

}
