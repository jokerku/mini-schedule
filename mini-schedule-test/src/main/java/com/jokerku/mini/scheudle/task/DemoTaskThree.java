package com.jokerku.mini.scheudle.task;

import com.jokerku.mini.schedule.annotation.MiniScheduled;
import org.springframework.stereotype.Component;


//@Component("demoTaskThree")
public class DemoTaskThree {

//    @MiniScheduled(cron = "0 0 9,13 * * *", desc = "03定时任务执行测试：taskMethod01", autoStartUp = false)
    public void taskMethod01() {
        System.out.println("03定时任务执行测试：taskMethod01");
    }

//    @MiniScheduled(cron = "0 0/30 8-10 * * *", desc = "03定时任务执行测试：taskMethod02", autoStartUp = false)
    public void taskMethod02() {
        System.out.println("03定时任务执行测试：taskMethod02");
    }

}
